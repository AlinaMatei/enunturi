-----------------2020------------
---V0---
JS->
# Given the function `bowdlerize(input, dictionary)` where:
- `input` is a string (e.g. "This is a cat")
- `dictionary` is a vector containing strings.

# Complete the following tasks:
- `input` should be of type `string`. If another type is given an `Error` is thrown with the message `Input should be a string`; (0.5 pts)
- `dictionary` is an array of `string`. If at least an element is not a `string` an `Error` is thrown with the message `Invalid dictionary format`; (0.5 pts)
- If `dictionary` contains words, they will be replaced in `input` with the first letter followed by a series of  `*` characters followed by the last letter. The length of the resulting word will be the same as the original (e.g. 'test' will become 't**t'); (0.5 pts)
- A new string wil be returned, with `input` remaining unmodified; (0.5 pts)
- The function also returns the correct result for words starting with a capital letter. (0.5 pts)

REACT->
# Having the following application created with `create-react-app` add a `Company` component and modify `CompanyList` so that:
- the app renders correctly (0.5 pts);
- `CompanyList` is rendered as a child of `App` (0.5 pts);
- `CompanyList` is rendered as a list of `Company` (0.5 pts);
- `Company` has a property called item containing the company it's supposed to render (0.5 pts);
- `Company` can be deleted via a button with the label `delete` (0.5 pts);

COMPANY - COMPANY FORM- COMPANY LIST
renders a list of companies
valid props on company
delete a company

REST->
# Having the following application developed using NodeJS, complete the `POST` method on path `/ships` :

- If no request body is present you should return a json with the following format: `{"message": "body is missing"}`. Response status code should be: `400`; (0.5 pts)
- If the request body properties are missing you should return a json with the following format: `{"message": "malformed request"}`. Response status code should be: `400`; (0.5 pts)
- Displacement should be over 1000, otherwise the server should return a message with the following format: `{"message": "displacement should be over 1000"}`. Response status code should be: `400`; (0.5 pts)
- If the ship is valid, it should be added and a response with the code `201` should be returned. The response body should be `{"message": "created"}`;(0.5 pts)
- If a `GET /ships` request is performed subsequently, the body should contain 11 `ships`, including the one previoulsy added; (0.5 pts)


SIMPLE->
# Complete the following tasks:
- the file `index.html`, which contains the text `A simple app` should be delivered from the server as static content (0.5 pts);
- a button with the id `load` exists in the page and can be clicked (0.5 pts);
- when the button with the id `load` is clicked, a list of cars should be fetched from the server; cars with the color `red` loaded in the table with the id `main` with a `tr` for each car (0.5 pts);
- the table contains a `tr` for each car loaded from the server (0.5 pts);
- only `red` cars are shown (0.5 pts);

---V1---

JS->
# Given the function `calculateFrequencies(input, stopWords)` where:
- `input` is a string (e.g. "This is an orange cat")
- `stopWords` is a vector containing strings.

# Complete the following tasks:
- `input` should be of type `string` or `String`. If another type is given an `Error` is thrown with the message `Input should be a string or a String`; (0.5 pts)
- `dictionary` is an array of `string` or `String`. If at least an element is not a `string` an `Error` is thrown with the message `Invalid dictionary format`; (0.5 pts)
- the function will calculate the relative frequencies of words in input and return a dictionary containing words as keys and frequencies as values (e.g. for the string 'orange cat' the result will be {orange : 0.5, cat : 0.5}); (0.5 pts)
- if stopWords contains any words, they will be ignored in the result (e.g. for the string 'the orange cat' with 'the' defined as a stopword the result will be {orange : 0.5, cat : 0.5}); (0.5 pts)
- the function also returns the correct result for words starting with a capital letter, which are considered identical to their lowercase variant. (0.5 pts)

REACT->
# Having the following application created with `create-react-app` modify `Company` component and modify `CompanyList` so that:
- the app renders correctly (0.5 pts);
- `CompanyList` is rendered as a list of `Company` and each `Company` has a button labeled `edit`(0.5 pts);
- If the edit button is clicked on a `Company` it goes into edit mode (0.5 pts);
- If a `Company` is in edit mode and the button labeled `cancel` is clicked, it goes into view mode (0.5 pts);
- A company can be saved and the changes are reflected in the company list (0.5 pts);

renders a list of companies with edit buttons
goes to edit mode upon clicking
can return to view mode
can save a company
COMPANY -> COMPANY LIST

REST->
# Having the following application developed using NodeJS, complete the `PUT` and `DELETE` methods on path `/ships/id` :

- If attempting to update a non existent ship the response should be `{"message": "not found"}`. Response status code should be: `404`; (0.5 pts)
- If correctly updating an existent ship the response should be: `{"message": "accepted"}`. Response status code should be: `202`; (0.5 pts)
- A subsequent request for the edited ship should show the modifications. Response status code should be: `200`; (0.5 pts)
- If correctly deleting an existent ship the response should be: `{"message": "accepted"}`. Response status code should be: `202`; (0.5 pts)
- A subsequent request for the deleted ship should return `{"message": "not found"}`. Response status code should be: `404`; (0.5 pts)

SIMPLE->

# Given the server `server.js` and the file `index.html` in the `public` directory:

# Complete the following tasks:
- the file `index.html`, which contains the text `A simple app` should be delivered from the server as static content (0.5 pts);
- a button with the id `reload` exists in the page and can be clicked (0.5 pts);
- when the button with the id `reload` is clicked with nothing in the filter, all elements are returned(0.5 pts);
- when the button with the id `reload` is clicked with `red` in the filter, elements with color `red` are returned(0.5 pts);
- when the button with the id `reload` is clicked with  a filter color which does not match anything an empty list is returned(0.5 pts); (0.5 pts);

------------TUTUROING--------------

---V0---
JS->

 - funcția distance primește ca parametrii două array-uri
 - fiecare element poate apărea cel mult o dată într-un array
 - distanța dintre cele 2 array-uri este numărul de elemente diferite dintre ele
 - dacă parametrii nu sunt array-uri se va arunca o excepție ("InvalidType")


REACT->
renders a list of robots
valid props on robot
delete a robot
ROBOT -> ROBOT LIST


REST->
IN POST
	// should add an author
	// author names and emails cannot be empty
	// author addresses can be empty
	// author emails have to be validated as email addresses
IN GET
	// should get all authors

SIMPLE->
/////////////////



---V1---
JS->
/*
Define a Widget object type is defined
The decorate function adds to Widget a method called enhance which increases the size of a widget with n
If the parameter is not a number an exception is thrown ("InvalidType")
The method also works on already declared Widgets
*/

REST->
IN DELETE->
	// add the method to delete an author
	// a non existant author cannot be deleted
IN PUT->
	// add the method to modify an author
	// a non existant author cannot be updated
	// only defined fields should be updated

REACT->
renders a robot form
valid props on robot form
add a robot and it exists on the page
correct robot


---V2---

JS->
# Having the `function applyDiscount(vehicles, discount)`, complete the following tasks:

- Function should return a Promise; (0.5 pts)
- If `discount` is not a number, the function should `reject` an `Error` with the message `Invalid discount`; (0.5 pts)
- `vehicles` is an array that contains objects with the following format: `{make: string, price: number}` (Example: [{make: "Audi A5", price: 15000}]). If an array with invalid objects is passed then the function should `reject` an `Error` with the message `Invalid array format`; (0.5 pts)
- Function should `reject` a `string` with the value `Discount too big` if `discount` is greater than 50% of the min price from `vehicles` array; (0.5 pts)
- Function should `resolve` an array with applied discount to each `vehicle price`; (0.5 pts)

REACT->
# Having the following application created with `create-react-app` complete the following tasks:
- `AddProduct` component should be rendered inside `ProductList` component;
- `AddProduct` component should contain 3 inputs with `id` and `name`: `name`, `category`, `price`;
- `AddProduct` component should contain an input of type `button` with the value `add product`, used to trigger `addProduct` method;
- `AddProduct` component inside `ProductList` should contain a `props` called `onAdd`;
- When pressing `add product` a new item should be displayed in `ProductList` component;

REST->

# Having the following application developed using NodeJS, complete the `POST` method on path `/students` :

- If no request body is present you should return a json with the following format: `{message: "Body is missing"}`. Response status code should be: `500`;
- If the request body properties are missing you should return a json with the following format: `{message: "Invlid body format"}`. Response status code should be: `500`;
- Student age should be positive, otherwise return a json with the following format: `{message: "Age should be a positive number"}`. Response status code should be: `500`; 
- If the student already exists in the array. Return a json with the following format: `{message: "Student already exists"}`.Response status code should be: `500`. Checking is done by `name`;
- If the request body is good, student should be added in the array and return a json with the following format: `{message: "Created"}`.Response status code should be: `201`;



---------2019-----------

---V0---
JS->
 - the distance function receives as parameters two arrays
 - each element can appear in each array at most once; any duplicates are ignored
 - the distance between the 2 arrays is the number of different elements between them
 - if the parameters are not arrays an exception is thrown ("InvalidType")

REACT->
renders a list of robots
valid props on robot
delete a robot

REST->
POST->

	// TODO: implement the endpoint
	// should add an author
	// author names and emails cannot be empty
	// author addresses can be empty
	// author emails have to be validated as email addresses
	// author emails have to be valid
GET->
// should get all authors


---V1---

JS->
Input should be a string
Input should have at least 6 characters
function textProcessor(input, tokens)

REACT->
CAR / CARLIST
The added car should be validated
AddCar component should be rendered
AddCar should contain 3 inputs
AddCar should contain a button to trigger addCar method
AddCar should contain props onAdd

REST->
# Having the following application developed using NodeJS, complete the `POST` method on path `/products` :

- If no request body is present you should return a json with the following format: `{message: "Body is missing"}`. Response status code should be: `500`;
- If the request body properties are missing you should return a json with the following format: `{message: "Invlid body format"}`. Response status code should be: `500`;
- Product price should be positive, otherwise return a json with the following format: `{message: "Price should be a positive number"}`. Response status code should be: `500`; 
- If the product already exists in the array. Return a json with the following format: `{message: "Product already exists"}`.Response status code should be: `500`;
- If the request body is good, product should be added in the array and return a json with the following format: `{message: "Created"}`.Response status code should be: `201`;

---V2---

JS->
Define a Widget object type is defined
The decorate function adds to Widget a method called enhance which increases the size of a widget with n
If the parameter is not a number an exception is thrown ("InvalidType")
The method also works on already declared Widgets
REACT->
renders a robot form
valid props on robot form
add a robot and it exists on the page
correct robot
REST->
PUT:
	// TODO: implement the function
	// add the method to modify an author
	// a non existant author cannot be updated
	// only defined fields should be updated
DELETE:

	// TODO: implement the function
	// add the function to delete an author
	// a non existant author cannot be deleted


---V3---
JS->
function applyBonus(employees, bonus)
Fuction should return the expected values
Function should reject a message: "Bonus too small" if the bonus is not at least 10% of the biggest salary
Function should reject an Error with the message "Invalid array format" if array is not in the specified format
Function should reject an Error with the message "Invalid bonus" if bonus parameter is not a number'
Testing applyBonus function

REACT->
PRODUCT LIST
AddProduct component should be rendered
AddProduct should contain 3 inputs of type text
AddProduct should contain a button to trigger addProduct method
AddProduct should contain props onAdd
The added product should be validated

REST->
- If no request body is present you should return a json with the following format: `{message: "Body is missing"}`. Response status code should be: `500`;
- If the request body properties are missing you should return a json with the following format: `{message: "Invlid body format"}`. Response status code should be: `500`;
- Student age should be positive, otherwise return a json with the following format: `{message: "Age should be a positive number"}`. Response status code should be: `500`; 
- If the student already exists in the array. Return a json with the following format: `{message: "Student already exists"}`.Response status code should be: `500`. Checking is done by `name`;
- If the request body is good, student should be added in the array and return a json with the following format: `{message: "Created"}`.Response status code should be: `201`;


---V4---
JS->
There is an object type called Bird
Define the Penguin type
A penguin is a child type for Bird and has an additional method called swim(distance)
A penguin cannot be created without a name of type string
A penguin cannot fly and will say that if asked
A penguin can make a nest via its parent's method
See the tests for the accurate format of messages

REACT->
ROBOT LIST
renders a list of robots with edit buttons
goes to edit mode upon clicking
can return to view mode
can save a robot

REST->
get->
	// should get all authors
	// should allow for pagination with a pageNo and a pageSize possibly sent as query parameters
post-> 
Aparent nimic practic:
	try{
		let author = new Author(req.body)
		await author.save()
		res.status(201).json({message : 'created'})
	}
	catch(err){
		// console.warn(err.stack)
		res.status(500).json({message : 'server error'})		
	}


---V5---
JS->
# Having the `function applyDiscount(vehicles, discount)`, complete the following tasks:

- Function should return a Promise; (0.5 pts)
- If `discount` is not a number, the function should `reject` an `Error` with the message `Invalid discount`; (0.5 pts)
- `vehicles` is an array that contains objects with the following format: `{make: string, price: number}` (Example: [{make: "Audi A5", price: 15000}]).
- If an array with invalid objects is passed then the function should `reject` an `Error` with the message `Invalid array format`; (0.5 pts)
- Function should `reject` a `string` with the value `Discount too big` if `discount` is greater than 50% of the min price from `vehicles` array; (0.5 pts)
- Function should `resolve` an array with applied discount to each `vehicle price`; (0.5 pts)

REACT->
STUDENT LIST
AddStudent component should be rendered
AddStudent should contain 3 inputs of type text
AddStudent should contain a button to trigger addStudent method
AddStudent should contain props onAdd
The added student should be validated

REST->
Having the following application developed using NodeJS, complete the `POST` method on path `/cars` :

- If no request body is present you should return a JSON with the following format: `{message: "Body is missing"}`. Response status code should be: `500`;
- If the request body properties are missing you should return a JSON with the following format: `{message: "Invlid body format"}`. Response status code should be: `500`;
- Car price should be positive, otherwise return a JSON with the following format: `{message: "Price should be a positive number"}`. Response status code should be: `500`; 
- If the car already exists in the array. Return a JSON with the following format: `{message: "Product already exists"}`.Response status code should be: `500`. Difference is made by the model;
- If the request body is good, car should be added in the array and return a JSON with the following format: `{message: "Created"}`.Response status code should be: `201`;


---V6---

JS->
/*
 - the capitalize function receives as parameters a string and an array
 - the dictionary (the array) contains a series of words
 - in the initial text the words are separated by space
 - each dictionary term has to appear capitalized in the result
 - the result is a new string without modifying the initial one
 - if the text is not string or the dictionary not an array of strings an exception is thrown (message is TypeError)
function capitalize(text, dictionary)
*/

REST->
get->
	// TODO: implement the function
	// should get all authors
	// should allow for filtering based on address and email (filters are called address and email and are sent as query parameters)
POST->
	try{
		let author = new Author(req.body)
		await author.save()
		res.status(201).json({message : 'created'})
	}
	catch(err){
		// console.warn(err.stack)
		res.status(500).json({message : 'server error'})		
	}

REACT->
ROBOT / ROBOT DETAILS / ROBOT LIST
renders a list of robots with select buttons
valid props on robot
select a robot
cancel selection

---V7---
JS->

# Having the `function addTokens(input, tokens)` where:
- `input` is a string that could contain "..." for example: Subsemnatul ..., dominiciliat in ...;
- `tokens` is an array with token names .
- The function should replace each `...` from `input` with the values from `tokens` in the following format `${tokenName}`;

# Complete the following tasks:

- `input` should be a `string`. If other type is passed throw an `Error` with the message `Input should be a string`; (0.5 pts)
- `input` should be at least 6 characters long. If `input` length is less than 6 throw an `Error` with the message `Input should have at least 6 characters`; (0.5 pts)
- `tokens` is an array with elements with the following format: `{tokenName: string}`. If this format is not respected throw an `Error` with the following message `Invalid array format`; (0.5 pts)
- If `input` don't contain any `...` return the initial value of `input`; (0.5 pts)
- If `input` contains `...`, replace them with the specific values and return the new `input`; (0.5 pts)

REST->


# Having the following application developed using NodeJS, complete the `POST` method on path `/students` :

- If no request body is present you should return a json with the following format: `{message: "Body is missing"}`. Response status code should be: `500`;
- If the request body properties are missing you should return a json with the following format: `{message: "Invlid body format"}`. Response status code should be: `500`;
- Student age should be positive, otherwise return a json with the following format: `{message: "Age should be a positive number"}`. Response status code should be: `500`; 
- If the student already exists in the array. Return a json with the following format: `{message: "Student already exists"}`.Response status code should be: `500`. Checking is done by `name`;
- If the request body is good, student should be added in the array and return a json with the following format: `{message: "Created"}`.Response status code should be: `201`;

REACT->
EMPLOYEE EMPLOYEE LIST ADDEMPLOYEE
AddEmployee component should be rendered
AddEmployee should contain 3 inputs of type text
AddEmployee should contain a button to trigger addEmployee method
AddEmployee should contain props onAdd
The added employee should be validated


---V8---
JS->
 - the translate function receives as parameters a string and an object
 - the function throws exceptions if the types are not the required ones (message is "InvalidType")
 - the dictionary object has in its keys the inital values and in its values the translation of the key
 - the values in the dictionary are strings
 - the function replaces each dictinary key found in the initial text with the value in the dictionary corresponding to the key
function translate(text,dictionary)

REST->
POST->
	// TODO: implementați funcția
	// ar trebui să adauge o carte la un autor
	// TODO: implement the function
	// should add a book to an author
GET->
	// TODO: implement the function
	// should list all of an authors' books


REACT->
ROBOT . ROBOT LIST
renders a list of robots for no filters
renders a list based on a name filter
renders a list based on a type filter
renders a list based on both filters

---V9---
JS->

# Having the `function applyBlackFriday(products, discount)` where:
- `products` is an array of objects with the following format {name: string, price: number};
- `discount` is a number that represents the percentage of discount to apply to the products price.
- The function should return an array with applied discount to each product

# Complete the following tasks:

- Function should return a promise; (0.5 pts)
- `discount` should be a number, otherwise `reject` the promise with an `Error` with the message `Invalid discount`; (0.5 pts)
- `discount` should be greater than 0 and less equals than 10, otherwise `reject` the promise with an `Error` with the message `Discount not applicable`; (0.5 pts)
- `products` should contain elements in the specified format, otherwise `reject` an `Error` with the message `Invalid array format`; (0.5 pts)
- Function should return the new array with the discounted products prices; (0.5 pts)

REST->

 Having the following application developed using NodeJS, complete the `POST` method on path `/cars` :

- If no request body is present you should return a JSON with the following format: `{message: "Body is missing"}`. Response status code should be: `500`;
- If the request body properties are missing you should return a JSON with the following format: `{message: "Invlid body format"}`. Response status code should be: `500`;
- Car price should be positive, otherwise return a JSON with the following format: `{message: "Price should be a positive number"}`. Response status code should be: `500`; 
- If the car already exists in the array. Return a JSON with the following format: `{message: "Product already exists"}`.Response status code should be: `500`. Difference is made by the model;
- If the request body is good, car should be added in the array and return a JSON with the following format: `{message: "Created"}`.Response status code should be: `201`;

REACT->
CUPON CUPON LIST

# Having the following application created with `create-react-app` complete the following tasks:
- `AddCoupon` component should be rendered inside `CouponList` component;
- `AddCoupon` component should contain 3 inputs with `id` and `name`: `category`, `discount`, `availability`;
- `AddCoupon` component should contain an input of type `button` with the value `add coupon`, used to trigger `addCoupon` method;
- `AddCoupon` component inside `CouponList` should contain a `props` called `onAdd`;
- When pressing `add coupon` a new item should be displayed in `CouponList` component;


---V10---

JS->

# Having the `function processString(input)`, which initially splits the `input` string into `tokens` based on space, complete the following tasks:

- If any `token` is not a number, the function should throw an `Error` 
- If `token` is not a number, the function should throw an `Error` with the message `Item is not a number`; (0.5 pts)
- If `input` is empty the function should return 100; (0.5 pts)
- Odd `tokens` are ignored; (0.5 pts)
- The function should return 100 minus the sum of all even `tokens`; (0.5 pts)

REST->
# Having the following application developed using NodeJS, complete the `POST` method on path `/students` :

- If no request body is present you should return a json with the following format: `{"message": "body is missing"}`. Response status code should be: `400`; (0.5 pts)
- If the request body properties are missing you should return a json with the following format: `{"message": "malformed request"}`. Response status code should be: `400`; (0.5 pts)
- Age  should be positive, otherwise return a json with the following format: `{"message": "age should be a positive number"}`. Response status code should be: `400`; (0.5 pts)
- If the student is valid, it should be added and a reponse with the code `201` should be returned. The response body should be `{"message": "created"}`;(0.5 pts)
- If a `GET /students` request is performed, the body should contain 11 `students`, including the one previoulsy added; (0.5 pts)

REACT->
# Having the following application created with `create-react-app` complete the following tasks:
- `AddTask` component should be rendered inside `TaskList` component;
- `AddTask` component should contain 3 inputs with the following properties `id` and `name` having the following values: `task-name`, `task-priority`, `task-duration`;
- `AddTask` component should contain an input of type `button` with the `value` property `add task`, used to trigger `addTask` method;
- `AddTask` component inside `TaskList` should contain a `props` called `taskAdded`;
- When pressing `add task` button a new item should be displayed in `TaskList` component;

TASK -TASTK LIST - ADDTASK
 AddTask component should be rendered
AddTask component should contain 3 inputs. Please check input ids
AddTask component should contain a button to trigger addTask method
AddTask component should contain props taskAdded
The added task should be validated


---V11---

JS->
# Having the `function applyDiscount(vehicles, discount)`, complete the following tasks:

- Function should return a Promise; (0.5 pts)
- If `discount` is not a number, the function should `reject` an `Error` with the message `Invalid discount`; (0.5 pts)
- `vehicles` is an array that contains objects with the following format: `{make: string, price: number}` (Example: [{make: "Audi A5", price: 15000}]). If an array with invalid objects is passed then the function should `reject` an `Error` with the message `Invalid array format`; (0.5 pts)
- Function should `reject` a `string` with the value `Discount too big` if `discount` is greater than 50% of the min price from `vehicles` array; (0.5 pts)
- Function should `resolve` an array with applied discount to each `vehicle price`; (0.5 pts)

REST->
# Having the following application developed using NodeJS, complete the `PUT` method on path `/students/:id` :

- If no request body is present you should return a json with the following format: `{"message": "body is missing"}`. Response status code should be: `400`; (0.5 pts)
- If the request body properties are missing you should return a json with the following format: `{"message": "malformed request"}`. Response status code should be: `400`; (0.5 pts)
- Only an existing student can be modified. Otherwise, the response should be : `{message: "not found"}`. Response status code should be: `404`; (0.5 pts)
- If the student exists and the body is valid, it should be modified and a reponse with the code `202` should be returned. The response body should be `{"message": "accepted"}`; (0.5 pts)
- If a `GET /students` request is performed, the body should contain 10 `students`, including the one previoulsy modified; (0.5 pts)

REACT->
- `AddBook` component should be rendered inside `BookList` component;
- `AddBook` component should contain 3 inputs with the following properties `id` and `name` having the following values: `book-title`, `book-type`, `book-price`;
- `AddBook` component should contain an input of type `button` with the `value` property `add book`, used to trigger `handleAdd` method;
- `AddBook` component inside `BookList` should contain a `props` called `itemAdded `;
- When pressing `add book` button a new item should be displayed in `BookList` component; 

BOOK-> BOOK LIST-> ADD BOOK
AddBook component should be rendered
AddBook component should contain 3 inputs. Please check input ids.
AddBook component should contain a button to trigger AddBook method
AddBook component should contain props taskAdded
The added item should be validated


---V12---
JS->
# Having the class `Shape` complete the following tasks:

- If `Shape` is instantiated directly and the `area` method is called, an `Error` with the message `not implemented` should be thrown; (0.5 pts) 
- A `Square` extending `Shape` should be defined. A `Square` can be instantiated by passing an object with a `width` property; (0.5 pts)
- Given a `Square`, its area should be correctly calculated; (0.5 pts)
- A `Circle` extending `Shape` should be defined. A `Circle` can be instantiated by passing an object with a `radius` property. Given a `Circle`, its area should be calculated correctly; (0.5 pts)
- A `Rectangle` extending `Shape` should be defined. A `Rectangle` can be instantiated by passing an object with a `width` property and a `height` property. Given a `Rectangle`, its area should be calculated correctly;  (0.5 pts)

REST->
aving the following application developed using NodeJS, complete the `GET` method on path `/students` and the `DELETE` method on path `/students/:id` :

- If a `GET /students` request is performed, a full list of students should be returned with a `200` response code; (0.5 pts)
- If a `GET /students?filter=etc` request is performed, a filtered list of students should be returned with a `200` response code; (0.5 pts)
- Only an existing student can be deleted. Otherwise, the response should be : `{message: "not found"}`. Response status code should be: `404`; (0.5 pts)
- If the student exists, it should be deleted and a reponse with the code `202` should be returned. The response body should be `{"message": "accepted"}`; (0.5 pts)
- If a `GET /students` request is performed, the body should contain 9 `students`, and should not contain the one previoulsy deleted; (0.5 pts)

REACT->
# Having the following application created with `create-react-app` complete the following tasks:
- `AddTransaction` component should be rendered inside `TransactionList` component;
- `AddTransaction` component should contain 3 inputs with the following properties `id` and `name` having the following values: `transaction-number`, `transaction-type`, `transaction-amount`;
- `AddTransaction` component should contain an input of type `button` with the `value` property `add transaction`,used to trigger `addTransaction` method;
- `AddTransaction` component inside `TransactionList` should contain a `props` called `itemAdded`;
- When pressing `add transaction` button a new item should be displayed in `TransactionList` component; 

TRANSACTION -> TRANSACTION LIST -> TRANSACTION ADD
AddTransaction component should be rendered
AddTransaction component should contain 3 inputs. Please check input ids.
AddTransaction component should contain a button to trigger AddTransaction method
AddTransaction component should contain props taskAdded
The added item should be validated


---V13---

JS->
# Having the `function toCamelCase(input)`, complete the following tasks:

- The function should throw an `Error` with the message `Input must be a string primitive or a string object` if input is number; (0.5 pts)
- The function should throw an `Error` with the message `Input must be a string primitive or a string object` if input is undefined; (0.5 pts)
- Given a  `-` separated input, the function should return it in camelCase (with a lower first letter) (0.5 pts)
- Given a  `_` separated input, the function should return it in camelCase (with a lower first letter) (0.5 pts)
- Given a  `space` separated input, the function should return it in camelCase (with a lower first letter) (0.5 pts)

REST->

# Having the following application developed using NodeJS, complete the `POST` method and the `GET` method on path `/cars` :

- If a `GET /cars?filter=GT` request is performed, a filtered list of cars should be returned with a `200` response code; (0.5 pts)
- If no request body is present you should return a json with the following format: `{"message": "body is missing"}`. Response status code should be: `400`; (0.5 pts)
- If the request body properties are missing you should return a json with the following format: `{"message": "malformed request"}`. Response status code should be: `400`; (0.5 pts)
- Year  should be a number greater than `1860`, otherwise return a json with the following format: `{"message": "year should be > 1860"}`. Response status code should be: `400`; (0.5 pts)
- If the `car` is valid, it should be added and a reponse with the code `201` should be returned. The response body should be `{"message": "created"}`


REACT->
# Having the following application created with `create-react-app` complete the following tasks:
- `AddVacation` component should be rendered inside `VacationList` component;
- `AddVacation` component should contain 3 inputs with the following properties `id` and `name` having the following values: `vacation-destination`, `vacation-location-type`, `vacation-price`;
- `AddVacation` component should contain an input of type `button` with the `value` property `add vacation`, used to trigger `addVacation` method;
- `AddVacation` component inside `VacationList` should contain a `props` called `itemAdded`;
- When pressing `add vacation` button a new item should be displayed in `VacationList` component; 

VACATION - VACATIONLIST - ADDVACATION
AddVacation component should be rendered
AddVacation component should contain 3 inputs. Please check input ids.
AddVacation component should contain a button to trigger AddVacation method
AddVacation component should contain props taskAdded
The added item should be validated













